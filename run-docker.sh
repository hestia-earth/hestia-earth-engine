#!/bin/sh
docker build --progress=plain \
  -t hestia-earth-engine:latest \
  .
docker run --rm \
  --name hestia-earth-engine \
  -v ${PWD}:/app \
  hestia-earth-engine:latest python run.py "$@"
