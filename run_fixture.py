# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Run `python run.py boundary samples/boundary-raster-period.json`
from dotenv import load_dotenv
load_dotenv()


import sys
import json
from hestia_earth.earth_engine import init_gee, run


def main(args):
    with open(args[0]) as f:
        data = json.load(f)

    init_gee()
    results = run(data)
    print(results)

    # make sure number of results matches queries
    collections = data.get('collections', [])
    locations = data.get('coordinates') or data.get('boundaries') or data.get('gadm-ids')

    print('nb locations', len(locations))
    print('nb collections', len(collections))
    print('nb results', len(results))

    assert len(results) == len(locations) * len(collections)


if __name__ == "__main__":
    main(sys.argv[1:])
