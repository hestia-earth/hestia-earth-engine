# usage: `python scripts/land_transformation.py "BRA.5.39.1_1" cropland 2000 20`
# where `2000` is the target year and `20` is the comparison year
import sys
import time
from functools import reduce
import ee

COLLECTION = 'users/hestiaplatform/anthromes'
GADM_COLLECTION_PREFIX = 'users/hestiaplatform/gadm36_'
SITE_TYPE_TO_MASK = {
    'cropland': [21, 22, 23, 31, 32, 33],
    'forest': [51, 52, 53, 61],
    'permanent pasture': [24, 34, 41, 42, 43]
}


def current_time(): return int(round(time.time() * 1000))


def _id_to_level(id: str): return id.count('.')


def _load_region(gadm_id: str):
    level = _id_to_level(gadm_id)
    id = gadm_id.replace('GADM-', '')
    return ee.FeatureCollection(f"{GADM_COLLECTION_PREFIX}{level}").filterMetadata(f"GID_{level}", 'equals', id)


def _closes_year_delta(year: int, delta: int):
    return round((year - delta) / 10) * 10


def filter_by_year(region: ee.FeatureCollection, year: int, delta: int, site_type: str):
    from_year = str(_closes_year_delta(year, delta))
    to_year = str(year)

    from_image = ee.Image(
        ee.ImageCollection(COLLECTION).filterDate(f"{from_year}-01-01", f"{from_year}-12-31").first()
    ).clip(region)
    to_image = ee.Image(
        ee.ImageCollection(COLLECTION).filterDate(f"{to_year}-01-01", f"{to_year}-12-31").first()
    ).clip(region)

    return from_image.updateMask(select_mask(to_image, site_type))


def select_mask(image: ee.Image, site_type: str):
    masks = SITE_TYPE_TO_MASK[site_type]
    return image.updateMask(
        reduce(lambda prev, curr: prev.Or(image.eq(curr)), masks[1:], image.eq(masks[0]))
    )


def _reduce_by_region(image: ee.Image, region: ee.FeatureCollection):
    return image.reduceRegion(
        reducer=ee.Reducer.sum(),
        geometry=region.geometry(),
        bestEffort=True
    )


def calculate_total_size(region: ee.FeatureCollection, land_image: ee.Image):
    area = _reduce_by_region(land_image.updateMask(land_image.gt(1).And(land_image.lt(100))), region)
    return ee.Number(area.get('b1'))


def calculate_proportion(region: ee.FeatureCollection, land_image: ee.Image, total_size: ee.Number, site_type: str):
    area = _reduce_by_region(select_mask(land_image, site_type), region)
    return ee.Number(area.get('b1')).divide(total_size).getInfo()


def run_calculations(gadm_id: str, year: int, delta: int, site_type: str):
    region = _load_region(gadm_id)
    timeperiod_image = filter_by_year(region, year, delta, site_type)
    total_size = calculate_total_size(region, timeperiod_image)
    from_year = _closes_year_delta(year, delta)

    now = current_time()
    proportion = calculate_proportion(region, timeperiod_image, total_size, 'cropland')
    print(f"proportion of {site_type} in {year} that was cropland in {from_year}", proportion)
    print('time', current_time() - now)

    now = current_time()
    proportion = calculate_proportion(region, timeperiod_image, total_size, 'permanent pasture')
    print(f"proportion of {site_type} in {year} that was permanent pasture in {from_year}", proportion)
    print('time', current_time() - now)

    now = current_time()
    proportion = calculate_proportion(region, timeperiod_image, total_size, 'forest')
    print(f"proportion of {site_type} in {year} that was forest in {from_year}", proportion)
    print('time', current_time() - now)


def main(args):
    ee.Initialize(ee.ServiceAccountCredentials('testlp@hestia-earth.iam.gserviceaccount.com', 'ee-credentials.json'))

    gadm_id = args[0]
    site_type = args[1]
    year = int(args[2])
    delta = int(args[3])
    run_calculations(gadm_id, year, delta, site_type)


if __name__ == "__main__":
    main(sys.argv[1:])
