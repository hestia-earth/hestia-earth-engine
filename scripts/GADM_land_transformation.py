# install: `pip install -r scripts/requirementx.txt`
# usage: `python scripts/GADM_land_transformation.py <GADM level or leave empty for 0 - country>`
import sys
import os
import time
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
import ee
from hestia_earth.schema import SiteSiteType

COLLECTION = 'users/hestiaplatform/anthromes'
BAND_NAME = 'b1'
GADM_COLLECTION_PREFIX = 'users/hestiaplatform/gadm36_'
SITE_TYPE_TO_MASK = {
    SiteSiteType.CROPLAND: [21, 22, 23, 31, 32, 33],
    SiteSiteType.FOREST: [51, 52, 53, 61],
    SiteSiteType.PERMANENT_PASTURE: [24, 34, 41, 42, 43],
    SiteSiteType.OTHER_NATURAL_VEGETATION: [54, 62, 63]
}
YEARS = [
    1900, 1910, 1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990,
    2000, 2010
]
DELTAS = {
    20: YEARS,  # start at 1900 + 20
    100: [1850, 1860, 1870, 1880, 1890] + YEARS  # start at 1850 + 100
}
SITE_TYPES = [
    SiteSiteType.CROPLAND,
    SiteSiteType.FOREST,
    SiteSiteType.PERMANENT_PASTURE,
    SiteSiteType.OTHER_NATURAL_VEGETATION
]


def current_time(): return int(round(time.time() * 1000))


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def defaut_scale(image: ee.Image, band_name: str = None, default: int = 1):
    try:
        band_name = band_name or image.bandNames().getInfo()[0]
    except Exception:
        ''
    return image.select(band_name).projection().nominalScale().getInfo() if band_name else default


def _all_gadm_ids(level: str):
    with open(f"./scripts/GADM-ID-level{level}.txt", 'r') as f:
        return non_empty_list(f.read().split('\n'))


def _id_to_level(id: str): return id.count('.')


def _load_region(gadm_id: str):
    level = _id_to_level(gadm_id)
    id = gadm_id.replace('GADM-', '')
    return ee.FeatureCollection(f"{GADM_COLLECTION_PREFIX}{level}").filterMetadata(f"GID_{level}", 'equals', id)


def area_km2(geometry: ee.Geometry): return geometry.area().divide(1000 * 1000).getInfo()


def _closes_year_delta(year: int, delta: int): return round((year - delta) / 10) * 10


def filter_by_year(region: ee.FeatureCollection, from_year: int, to_year: int, site_type: SiteSiteType):
    from_image = ee.Image(
        ee.ImageCollection(COLLECTION).filterDate(f"{from_year}-01-01", f"{from_year}-12-31").first()
    ).clip(region)
    to_image = ee.Image(
        ee.ImageCollection(COLLECTION).filterDate(f"{to_year}-01-01", f"{to_year}-12-31").first()
    ).clip(region)

    return from_image.updateMask(select_mask(to_image, site_type))


def select_mask(image: ee.Image, site_type: SiteSiteType):
    masks = SITE_TYPE_TO_MASK[site_type]
    return image.updateMask(
        reduce(lambda prev, curr: prev.Or(image.eq(curr)), masks[1:], image.eq(masks[0]))
    )


def _size_image(image: ee.Image, region: ee.FeatureCollection):
    return ee.Number(image.reduceRegion(
        reducer=ee.Reducer.sum(),
        geometry=region.geometry(),
        scale=defaut_scale(image),
        bestEffort=True
    ).get(BAND_NAME))


def site_type_filepath(site_type: SiteSiteType, delta: int):
    filepath = f"scripts/region-{site_type.value.replace(' ', '_')}-landTransformation{delta}years-lookup.csv"
    other_site_types = [s for s in SITE_TYPES if s != site_type]

    if not os.path.exists(filepath):
        open(filepath, 'w+').writelines(f"{','.join(['term.id'] + [s.value for s in other_site_types])}\n")

    return filepath


def calculate_from_subregions(delta: int, to_site_type: SiteSiteType, gadm_id: str, filepath: str):
    print('processing subregions', delta, to_site_type, gadm_id)
    import pandas as pd
    df = pd.read_csv(filepath, index_col='term.id')
    df = df[df.index.str.startswith(gadm_id)]
    sub_region_ids = df.index.to_list()

    site_type_year_data = {}

    for sub_region_id in sub_region_ids:
        sub_region_size = area_km2(_load_region(sub_region_id).geometry())

        for from_site_type in df:
            values = df[from_site_type][sub_region_id].split(';')
            site_type_year_data[from_site_type] = site_type_year_data.get(from_site_type, {})

            for value in values:
                year, year_value = value.split(':')
                site_type_year_data[from_site_type][year] = site_type_year_data[from_site_type].get(year, [])
                site_type_year_data[from_site_type][year].append((float(year_value), sub_region_size))

    data = []
    for from_site_type, values in site_type_year_data.items():
        years_to_value = {}

        for year, year_values in values.items():
            total_weight = sum(weight for _v, weight in year_values)
            weighted_value = [value * weight for value, weight in year_values]
            years_to_value[year] = sum(weighted_value) / (total_weight if total_weight != 0 else 1)

        data.append(';'.join(f"{year}:{round(value / delta, 8)}" for year, value in years_to_value.items()))

    open(filepath, 'a+').writelines(f"{','.join([gadm_id] + data)}\n")


def calculate_data(values: list):
    delta, to_site_type, gadm_id, filepath = values
    print('processing', delta, to_site_type, gadm_id)
    other_site_types = [s for s in SITE_TYPES if s != to_site_type]

    region = _load_region(gadm_id)

    site_type_year_data = {}

    for to_year in DELTAS[delta]:
        try:
            from_year = _closes_year_delta(to_year, delta)
            # only calculate diff between dates we handle
            if from_year not in DELTAS[delta]:
                continue

            site_type_size = {}
            timeperiod_image = filter_by_year(region, from_year, to_year, to_site_type)

            for from_site_type in SITE_TYPES:
                size = _size_image(select_mask(timeperiod_image, from_site_type), region)
                site_type_size[from_site_type] = size

            # size might be over 100%, so normalize it
            total_size = reduce(lambda p, c: p.add(c), site_type_size.values(), ee.Number(0))
            for from_site_type, value in site_type_size.items():
                size_percent = value.divide(total_size).multiply(100).getInfo()
                site_type_year_data[from_site_type] = site_type_year_data.get(from_site_type, {})
                site_type_year_data[from_site_type][to_year] = size_percent
        except Exception as e:
            # skip year if there was an exception
            print('exception running', delta, to_site_type, gadm_id, to_year, str(e))
            continue

    data = []
    for site_type in other_site_types:
        years_to_value = site_type_year_data[site_type]
        data.append(';'.join(f"{year}:{round(value / delta, 8)}" for year, value in years_to_value.items()))

    open(filepath, 'a+').writelines(f"{','.join([gadm_id] + data)}\n")


def generate_data(level: str):
    gadm_ids = _all_gadm_ids(level)
    values = []
    for delta in DELTAS.keys():
        for site_type in SITE_TYPES:
            filepath = site_type_filepath(site_type, delta)
            file_content = open(filepath, 'r').read()
            for gadm_id in gadm_ids:
                # search for "GADM-GBR," instead of "GADM-GBR" since sub-regions might exist
                if (gadm_id + ',') in file_content:
                    # skip already generated data
                    continue
                # means it already has sub-regions - can calculate from them
                elif (gadm_id + '.') in file_content:
                    calculate_from_subregions(delta, site_type, gadm_id, filepath)
                else:
                    values.append([delta, site_type, gadm_id, filepath])

    with ThreadPoolExecutor() as executor:
        return list(executor.map(calculate_data, values))


def main(args: list):
    ee.Initialize(ee.ServiceAccountCredentials(None, 'ee-credentials.json'))
    level = args[0] if len(args) > 0 else 0
    generate_data(level=str(level))


# run GADM-CHL, GADM-CAN, GADM-USA
if __name__ == "__main__":
    main(sys.argv[1:])
