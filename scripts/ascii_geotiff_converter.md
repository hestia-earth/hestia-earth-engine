# ascii_to_raster()

This is a stand alone script used to convert ascii files into geotiffs.
This R script was used to convert the anthromes ascii files (from HYDE 3.2, see: https://dataportaal.pbl.nl/downloads/HYDE/HYDE3.2/) into geotiffs that were then uploaded to Google Earth Engine, under the "hestiaplatform" user.

## Install

1. Install R version `4.0.2` minimum
2. Install the libraries:
```bash
Rscript scripts/install_requirements.R
```

### Used packages

- Package `sp` (version = 1.4-5)
- Package `raster` (version = 3.4-10)
- Package `rgdal` (version = 1.5-23)

## Usage

1. Drop the ascii files in a `data` folder
2. Run the script:
```
Rscript ascii_geotiff_converter.R data output
```

This will convert any files found in the `data` folder and save them under `output` folder.

### Run with Docker

If you have Docker installed, you can follow theses steps:
1. Drop the ascii files in a `data` folder
2. Run the script:
```
./run-docker-script.sh Rscript scripts/ascii_geotiff_converter.R data output
```
