#!/bin/sh

# exit when any command fails
set -e

export PKG_DIR="python"

LIBRARY="earth_engine"
PKG_PATH="./layer/$PKG_DIR/lib/python3.9/site-packages/"

rm -rf "./layer/${PKG_DIR}" && mkdir -p "./layer/${PKG_DIR}"

PACKAGE_PATH="package.json"
PACKAGE_VERSION=$(cat $PACKAGE_PATH \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

# create requirements.txt
cp requirements.txt layer/requirements.txt

# remove utils package (in layer)
sed -i '/hestia_earth.utils.*/d' layer/requirements.txt || sed -i '' '/hestia_earth.utils.*/d' layer/requirements.txt

docker run --rm -v $(pwd):/var/task public.ecr.aws/sam/build-python3.9 pip install -r layer/requirements.txt -t $PKG_PATH

# copy the current library
rm -rf "${PKG_PATH}hestia_earth/${LIBRARY}/"
mkdir -p "${PKG_PATH}hestia_earth"
cp -R "./hestia_earth/${LIBRARY}/" "${PKG_PATH}hestia_earth/${LIBRARY}"
