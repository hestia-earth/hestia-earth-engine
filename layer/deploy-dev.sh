#!/bin/sh

set -e

STAGE=${1:-"dev"}
export REGION="us-east-1"

./layer/build.sh

# copy ee-credentials.json and exit if not found
cp ee-credentials.json ./layer/python/ee-credentials.json

./layer/deploy.sh $STAGE
