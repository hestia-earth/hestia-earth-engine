import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
fixtures_path = os.path.abspath(os.path.join(CURRENT_DIR, 'fixtures'))


def get_results(response: dict): return list(map(lambda f: f.get('properties'), response.get('features')))


def get_result(response: dict, key: str): return get_results(response)[0].get(key)
