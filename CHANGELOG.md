# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.4.7](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.6...v0.4.7) (2024-04-15)


### Features

* **gee:** handle querying between specific dates ([56c38f4](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/56c38f4b2ff550665f9c6c81f14e7940a45f289a))

### [0.4.6](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.5...v0.4.6) (2023-12-14)


### Bug Fixes

* **gee:** unmask data only for coordinates ([e42d1e1](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/e42d1e12e56c836e7cf6bbdb635003fb8d542f25))

### [0.4.5](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.4...v0.4.5) (2023-11-13)


### Bug Fixes

* **gee:** handle raster with missing data per year ([3fdae19](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/3fdae19182e61a67ebadd64b68f3d761f6944ec7))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.3...v0.4.4) (2023-11-06)


### Bug Fixes

* **gee:** handle missing values in raster files ([4c4fb72](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/4c4fb72e476697fb6ef7f9392a91fc1269ecd35a))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.2...v0.4.3) (2023-10-23)

### [0.4.2](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.1...v0.4.2) (2023-10-18)


### Bug Fixes

* change annual reducer with `reducer_annual` ([fafcda6](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/fafcda6de2b2ab4a2b75a6fc6f5b290eff07ff9b))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.4.0...v0.4.1) (2023-10-18)


### Bug Fixes

* handle single `reducer` in all `collections` ([e000aa9](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/e000aa987ced982dbe856625444a920da1ace438))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.3.1...v0.4.0) (2023-10-18)


### ⚠ BREAKING CHANGES

* Change `raster_by_period` to `raster`.

* handle multiple `raster` with multiple locations ([2c8f600](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/2c8f60042f66d095133572111fbad7a74182d367))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.3.0...v0.3.1) (2023-10-02)


### Features

* enable using Earth Engine for vector query if not enabled ([e606d9a](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/e606d9abd7fdc942b29982fec29fce27216c6985))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.2.2...v0.3.0) (2023-09-29)


### ⚠ BREAKING CHANGES

* Input data uses a list of collections and coordinates, boundaries or gadm-ids.
Please look at the readme for example.
* Data values are returned directly instead of geojson.
* Remove `get_size_km2` from main entrypoint.

* run vector data using geopandas ([83123e8](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/83123e8188bda82fe977e81c8fe47013649b3e5f))

### [0.2.2](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.2.1...v0.2.2) (2023-07-24)


### Features

* add `high_volume` param to use High Volume endpoint ([66cb628](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/66cb628b281c89409fe91116bb1a6e4688a17d8a))
* **gadm:** add method to get region by coordinates ([06b0ef3](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/06b0ef36e532f640e374c12b8ab47e705e10d653))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.2.0...v0.2.1) (2023-04-10)


### Features

* **gadm:** add method to return distance to coordinates in meters ([cffccb8](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/cffccb85e32472c6a1bd74f6747ab62b80a93a47))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.1.3...v0.2.0) (2022-12-30)


### ⚠ BREAKING CHANGES

* function `should_run` has been removed in favor of `get_size_km2`.

* replace `should_run` with function to return size in `km2` ([2bde546](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/2bde5460c3d83d205bdc663e763980821ab7ee8f))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.1.2...v0.1.3) (2022-10-04)


### Features

* add function to merge region geometries into a single one ([7c8bd91](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/7c8bd91891584bb923d87a8fc4f71742c39d747b))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.1.1...v0.1.2) (2022-06-02)

### [0.1.1](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.1.0...v0.1.1) (2022-04-06)


### Features

* **gee:** handle reducing by raster over multiple years ([3a298d6](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/3a298d68ccb156e1e60926a74303a7b43d8c8a2f))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.0.2...v0.1.0) (2022-02-28)


### ⚠ BREAKING CHANGES

* call `run(data)` and skip specifying `run_type`
* call `should_run` to check boundary/gadm size

* detect run type from data ([a1063c2](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/a1063c24f5246f9ae3c28905d1bae659e04dc2c7))
* export max area size check to `should_run` function ([53760c2](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/53760c232d73cfa9d05a894eb3a375c8ef954708))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-earth-engine/compare/v0.0.1...v0.0.2) (2022-02-11)

### 0.0.1 (2022-02-11)


### Features

* run by coordinates, boundary and gadm ([9b45ca0](https://gitlab.com/hestia-earth/hestia-earth-engine/commit/9b45ca0a222f4f64b2cb2796a9cbbc0b9842b3bf))
